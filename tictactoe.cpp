﻿#include "tictactoe.h"
int main() {
	tictactoe game;
	initboard(game.board);
	game.player1 = playerchoose("Player 1"); // Player1 play first and choose "x" or "o"
	game.cpu = aichoose(game.player1); //AI pick another character
	printboard(game.board);
	play(game.board, game.player1, game.cpu);
	printboard(game.board);

return 0;
}
