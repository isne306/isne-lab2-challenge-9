#ifndef TICTACTOE_H_
#define TICTACTOE_H_
#include <string>
#include <iostream>
#include <cstdlib>
using namespace std;

struct tictactoe {
	char board[3][3];
	char player1;
	char ai;
	char cpu;
};

void initboard (char board[3][3]) {
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			board[i][j] = '_';
		}
	}
}

//Prints board.
void printboard(char board[3][3]) {

	for (int i = 0; i < 3; i++) {
		cout << endl;
		for (int j = 0; j < 3; j++) {
			cout << "  ";
			cout << board[i][j];
		}
	}
	cout << endl << endl;
}

//Checks win for three in a row in all directions.
char check_win(char board[3][3]) {
	// Check horizontal, vertical and board [0][0]
	if ((board[0][0] != '_') && ((board[0][0] == board[0][1] && board[0][0] == board[0][2]) ||
		(board[0][0] == board[1][0] && board[0][0] == board[2][0] )||
		(board[0][0] == board[1][1] && board[0][0] == board[2][2]))) 
		return board[0][0];

	// Check horizontal, vertical & and [1][1]
	if ((board[1][1] != '_') && ((board[1][1] == board[1][0] && board[1][1] == board[1][2] )||
		(board[1][1] == board[0][1] && board[1][1] == board[2][1]) ||
		(board[1][1] == board[2][0] && board[1][1] == board[0][2]))) 
		return board[1][1];
	
	// Check horizontal, vertical & and [2][2]
	if ((board[2][2] != '_') && ((board[2][2] == board[0][2] && board[2][2] == board[1][2]) ||
		(board[2][2] == board[2][0] && board[2][2] == board[2][1])))
		return board[2][2];
	return 0;
}

//check possible position.
int maxmove(char board[3][3], char player1, char ai) {
	int bestmovescore = -9999;
	int scoremove = 0;
	if (check_win(board) == player1) {return 1000;}
	else if (check_win(board) == ai) {return -1000;}
	
	for (int r = 0; r < 3; r++) {
		for (int c = 0; c < 3; c++) {
			if (board[r][c] == '_') {
				board[r][c] = player1;
				scoremove = -(maxmove(board, ai, player1));
				board[r][c] = '_';
				if (scoremove >= bestmovescore) { bestmovescore = scoremove; }
			}
		}
	}

	if (bestmovescore == -9999 || bestmovescore == 0) {return 0;}
	else if (bestmovescore < 0) {return bestmovescore + 1;}
	else if (bestmovescore > 0) {return bestmovescore - 1;}
}
	
// Finds the best possible move.
int pickbestmove(char board[3][3], char player1, char ai) {
	int bestmovescore = -9999,bestrow = -9999,bestcol = -9999,scoremove = 0;
	for (int r = 0; r < 3; r++) {
		for (int c = 0; c < 3; c++) {
			if (board[r][c] == '_') {
				board[r][c] = player1; //Try test move_
				scoremove = -(maxmove(board, ai, player1));
				board[r][c] = '_'; //Put back test move.
				if (scoremove >= bestmovescore) {
					bestmovescore = scoremove;
					bestrow = r;
					bestcol = c;
				} 
			}
		}
	}
	return (10*bestrow + bestcol);
}

//Asks the user for move
void playermove(char board[3][3], char player) {
	while (1) {
		string srow, scol;
		int row = 0, col = 0;
		while (1) {
			cout << "Enter the row and the column: ";
			cin >> srow >>scol ;
			row = atoi(srow.c_str()); //Convert string to integer
			col = atoi(scol.c_str()); //Convert string to integer
			if (row >= 1 && row <= 3 && col >= 1 && col <= 3)
				break;
			cout << "You need to enter between 1 and 3." << endl;
		}
		if (board[row-1][col-1] == '_') {
			board[row-1][col-1] = player;
			break;
		}
		else
			cout << "Someone already played there." << endl << endl;
	}
}

// Asks the player to select a character.
char playerchoose (string s) {	
	while (1) {
		string choice;
		cout << s << ": Choose the character? (x or o) ";
		cin >> choice;
		if (choice.size() != 1) {
			cout << "You inputted more than one character. Please try again." << endl;
			continue;
		}
		cout << endl;
		if (choice[0] == 'o' || choice[0] == 'O') 
			{choice[0]= 'o';}
		else { 
			choice[0]= 'x';
			}
		return choice[0];
	}
}
//AI choose : If player1 choose 'x', AI will be 'o'. If player1 choose 'o', AI will be 'x' . If player1 chosse another, It's will set you to x.
char aichoose (char player1) {
	char cpuchar;
	cout << "Hello, I am the AI, I'm your compettitor." << endl;
	if (player1 == 'x' || player1 == 'X') 
		cpuchar = 'o';
	else 
		cpuchar = 'x';
	return cpuchar;
}

//Plays a game 
void play (char board[3][3], char player1, char cpu) {
	int moves = 0;
	while (moves < 9) {
		playermove(board, player1);
		moves++;
		printboard(board);
		if (check_win(board)) {
			cout  << " Player1 Won!" <<endl;
			exit(1);
		}
		if (moves == 9)
			break;
		int waymove = pickbestmove(board, cpu, player1);
		int row = waymove / 10;
		int col = waymove % 10;
		board[row][col] = cpu;
		moves++;
		printboard(board);

		if (check_win(board)) {
			cout << " You Lose! and AI Win" <<endl;
			exit(2);
		}
	}
	cout << "Draw!" << endl;
};

#endif